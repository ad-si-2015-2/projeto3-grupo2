package servidor_ws;

/**
 * @author Vitor Almeida
 */
public enum Estado {
  DEFESA,
  INERCIA;
}
