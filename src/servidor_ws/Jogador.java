package servidor_ws;

/**
 * @author Humberto Miranda
 */
public class Jogador {

  private float vida = 100;
  private String nome;
  private Estado estado = Estado.INERCIA;
  private int id;

  public Jogador() {}

  public float getVida() {
    return vida;
  }

  public void setVida(float vida) {
    this.vida = vida;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public Estado getEstado() {
    return estado;
  }

  public void setEstado(Estado estado) {
    this.estado = estado;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void receberGolpe() {
    vida -= isEmInercia() ? Jogada.randomHitIncercia() : Jogada.randomHitDefesa();
  }

  private boolean isEmInercia() {
    return estado.equals(Estado.INERCIA);
  }

  @Override
  public String toString() {
    return String.format("Jogador nome=%s, vida=%s, estado=%s", nome, vida, estado);
  }

}
