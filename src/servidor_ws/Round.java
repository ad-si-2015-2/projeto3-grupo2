package servidor_ws;

public class Round {

  private int vez = 0;

  public int getVez() {
    return vez;
  }

  public void setVez() {
    vez = (getVez() == 0) || (getVez() == 2) ? 1 : 2;
  }

  public int getInimigo() {
    return getVez() == 1 ? 2 : 1;
  }

  public int verificaGanhador(Jogador jogador1, Jogador jogador2) {
    if (jogador1.getVida() <= 0) {
      return 2;
    } else if (jogador2.getVida() <= 0) {
      return 1;
    }
    return 0;
  }

}
