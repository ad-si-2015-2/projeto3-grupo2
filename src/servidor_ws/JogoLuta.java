package servidor_ws;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

/**
 * @author Humberto Miranda
 */
@WebService
public class JogoLuta {
  List<Jogador> jogadores = new ArrayList<>();
  Round round = new Round();
  boolean jogoIniciado;

  /**
   * Registra o jogador e retorna para ele uma mgensagem de sucesso ou falha.
   * @param jogador a ser Registrado
   * @return Mensagem informando se foi registrado ou n�o
   */
  public String registrar(Jogador jogador) {
    if (jogadores.size() < 2) {
      jogador.setId(jogadores.size() + 1);
      jogadores.add(jogador);
      if (jogadores.size() == 2) {
        round.setVez();
        jogoIniciado = true;

        System.out.println(jogadores.get(0));
        System.out.println(jogadores.get(1));
      }
      return "Voce foi registado com o ID: " + jogador.getId();
    } else {
      return "Voce nao foi registrado no servidor.";
    }
  }

  /**
   * Retorna o ID do jogador
   * @param nome Nome do jogador
   * @return o ID do jogador caso exista, caso contr�rio retorna 0 (Zero)
   */
  public int getId(String nome) {
    for (Jogador jogador : jogadores) {
      if (jogador.getNome().equals(nome)) {
        return jogador.getId();
      }
    }
    return 0;
  }

  public boolean minhaVez(int id) {
    if (id == round.getVez()) {
      return true;
    } else {
      return false;
    }
  }

  public String getPontuacao() {
    String resp = "|===================================================|\r\n"
        + "|-----------         PLACAR     ----------|\r\n"
        + "|                                                   |\r\n"
        + "| ##################################################|\r\n";
    for (Jogador jogador : jogadores) {
      resp = resp + "| ########### " + jogador.getNome() + " :" + jogador.getVida() + " ###########|\r\n";
    }

    resp = resp + "|                                                   |\r\n"
        + "| ##################################################|\r\n";
    return resp;
  }

  public String jogada(int id, String cmd) {
    cmd = cmd.toUpperCase();
    int posicaoInimigo = 0;

    if (id == 1) {
      posicaoInimigo = 1;
    }

    id = id - 1;

    String resp = "";

    if (minhaVez(id + 1)) {
      for (Jogada jogada : Jogada.values()) {
        if (cmd.equals(jogada.getAtalho())) {
          if (cmd.equals("D")) {
            jogadores.get(id).setEstado(Estado.DEFESA);
          } else if (cmd.equals("P")) {
            return getPontuacao();
          } else {
            jogadores.get(posicaoInimigo).receberGolpe();
            jogadores.get(id).setEstado(Estado.INERCIA);
            resp = "Voce deu um(a) " + jogada.getDescricao();
          }
          round.setVez();
          return resp;
        }
      }
    }
    return "Nao eh sua vez, aguarde...";
  }

  public String verificaGanhador() {
    Jogador player1 = jogadores.get(0);
    Jogador player2 = jogadores.get(1);
    int vitoria = round.verificaGanhador(player1, player2);
    if (vitoria == 1) {
      return mensagemVencedor(player1.getNome());
    } else if (vitoria == 2) {
      return mensagemVencedor(player2.getNome());
    }
    return "";
  }

  private String mensagemVencedor(String nomeJogador) {
    return String.format("O jogador %s ganhou! \r\n Reinicie o jogo!", nomeJogador);
  }

  public String descricaoJogo() {
    return "\r\n" + "|===============================================================|\r\n"
        + "|-----------------         Comandos do Jogo     ----------------|\r\n"
        + "|                                                               |\r\n"
        + "| ##############################################################|\r\n"
        + "| #################                            #################|\r\n"
        + "| ################# C -> Chute                 #################|\r\n"
        + "| ################# S -> Soco                  #################|\r\n"
        + "| ################# D -> Defesa                #################|\r\n"
        + "| ################# P -> Pontua��o             #################|\r\n"
        + "| #################                            #################|\r\n"
        + "| ##############################################################|\r\n"
        + "|===============================================================|\r\n\r\n";
  }
}
