package servidor_ws;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Enum que representa as jogadas poss�veis.
 * @author Dayan Costa
 */
public enum Jogada {

  SOCO("S", "Soco", "Voce levou um soco!"),
  CHUTE("C", "Chute", "Voce levou um chute!"),
  ESQUIVA("E", "Esquiva", "Jogador esquivou-se"),
  PONTUACAO("P", "Pontua��o", "");

  private String atalho;
  private String descricao;
  private String mensagem;

  private Jogada(String atalho, String descricao, String mensagem) {
    this.atalho = atalho;
    this.descricao = descricao;
    this.mensagem = mensagem;
  }

  /**
   * @return hit aleat�rio entre 5 e 15 quando o inimigo estiver em estado de in�rcia
   */
  public static float randomHitIncercia() {
    int max = 15;
    int min = 7;
    return new Random().nextInt((max - min) + 1) + min;
  }

  /**
   * @return hit aleat�rio entre 1 e 7 quando o inimigo estiver em estado de defesa
   */
  public static float randomHitDefesa() {
    int max = 7;
    int min = 1;
    return new Random().nextInt((max - min) + 1) + min;
  }

  public String getAtalho() {
    return atalho;
  }

  public String getDescricao() {
    return descricao;
  }

  public String getMensagem() {
    return mensagem;
  }

  public static final List<Jogada> getJogadas() {
    return Collections.unmodifiableList(Arrays.asList(values()));
  }

  public static String random() {
    return getJogadas().get(new Random().nextInt(getJogadas().size())).getAtalho();
  }
}
