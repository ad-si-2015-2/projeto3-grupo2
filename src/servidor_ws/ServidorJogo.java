package servidor_ws;

import javax.xml.ws.Endpoint;

/**
 * @author Humberto Miranda
 */
public class ServidorJogo {
  public static void main(String[] args) {
    Endpoint.publish("http://localhost:8085/JogoLuta", new JogoLuta());
    System.out.println("Servidor iniciado");
  }
}
