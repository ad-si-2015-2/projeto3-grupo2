/* To change this template, choose Tools | Templates
 * and open the template in the editor. */
package cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import client_ws.Jogador;
import client_ws.JogoLuta;
import client_ws.JogoLutaService;

/**
 * @author Humberto Miranda
 */
public class ClienteJogo {

  public static void main(String[] args) throws InterruptedException, IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    JogoLutaService jas = new JogoLutaService();
    JogoLuta port = jas.getPort(JogoLuta.class);
    System.out.println("Informe seu nome: ");
    Jogador jogador = new Jogador();
    int id = 0;

    boolean jogando = false;

    try {
      jogador.setNome(reader.readLine());
      jogador.setVida(100);
      port.registrar(jogador);
      if (port.getId(jogador.getNome()) != 0) {
        System.out.println("Voc� foi conectado ao Servidor e seu ID �: " + port.getId(jogador.getNome()));
        jogando = true;
        id = port.getId(jogador.getNome());
      } else {
        System.out.println("Voc� n�o foi conectado ao Servidor. Provavelmente o Servidor esta lotado.");
        return;
      }
    } catch (IOException ex) {
      Logger.getLogger(ClienteJogo.class.getName()).log(Level.SEVERE, null, ex);
    }

    System.out.println(port.descricaoJogo());

    while (jogando) {
      Thread.sleep(1000);
      if (port.minhaVez(id)) {
        System.out.println("Sua vez de jogar!");
        System.out.println(port.jogada(id, reader.readLine()));
        String vencedor = port.verificaGanhador();
        if (!vencedor.equals("")) {
          System.out.println(vencedor);
          jogando = false;
        }
      }
    }
  }
}
