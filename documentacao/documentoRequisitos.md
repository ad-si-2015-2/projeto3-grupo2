## Documento de Requisitos


### 1. Identificação dos requisitos

Por convenção, a referência a requisitos é feita através do nome da subseção onde eles estão descritos, seguidos do identificador do requisito, de acordo com a especificação a seguir:
[nome da subseção. identificador do requisito]
Por exemplo, o `requisito` funcional [Autenticação do Usuário .RF016] deve estar descrito em uma subseção chamada “Autenticação do Usuário”, em um bloco identificado pelo número [RF016]. Já o requisito não-funcional [Confiabilidade.NF008] deve estar descrito na seção de requisitos não-funcionais de Confiabilidade, em um bloco identificado por **[NF008]**.
Os requisitos devem ser identificados com um identificador único. A numeração inicia com o identificador **[RF001]** ou **[NF001]** e prossegue sendo incrementada à medida que forem surgindo novos requisitos.


### 1.2 Prioridades dos requisitos

Para estabelecer a prioridade dos requisitos, nas seções 4 e 5, foram adotadas as denominações “essencial”, “importante” e “desejável”.

`Essencial` : é o requisito sem o qual o sistema não entra em funcionamento. Requisitos essenciais são requisitos imprescindíveis, que têm que ser implementados indispensavelmente.

`Importante` : é o requisito sem o qual o sistema entra em funcionamento, mas de  forma não satisfatória. Requisitos importantes devem ser implementados, mas, se não forem, o sistema poderá ser implantado e usado mesmo assim.

`Desejável` : é o requisito que não compromete as funcionalidades básicas do sistema, isto é, o sistema pode funcionar de forma satisfatória sem ele. Requisitos desejáveis podem ser deixados para versões posteriores do sistema, caso não haja tempo hábil para implementá-los na versão que está sendo especificada.


#### 3. Requisitos Funcionais

**[RF001]** Acesso livre

O Jogo é iniciado quando 2 jogadores se conectam ao sistema.

Prioridade: Essencial


**[RF002]** Inicio

O Jogo é iniciado quando 2 jogadores conectam.

Prioridade: Essencial

**[RF003]** Característica da Luta

Jogo multiusuário podendo ter 2 jogadores humanos, ou um jogador humano e um IA.
O jogo terá 3 rounds vencendo o jogador com 2 vitórias.
Cada lutador ataca uma vez, seu ataque pode ser também uma defesa.

Prioridade: Essencial

**[RF004]** Interface

A interface do sistema que o usuário irá interagir deverá ser um terminal

Prioridade: Importante



#### 4. Requisitos Não-Funcionais


**[NF001]** Usabilidade

A interface com o usuário é de vital importância para o sucesso do sistema. Principalmente por ser um sistema que não será utilizado diariamente, o usuário não possui tempo disponível para aprender como utilizar o sistema. O sistema terá uma interface amigável ao usuário primário sem se tornar cansativa aos usuários mais experientes.

Prioridade: Essencial


**[NF002]** Desempenho

Embora não seja um requisito essencial ao sistema, deve ser considerada por corresponder a um fator de qualidade de software.

Prioridade: Importante


**[NF003]** Implementação

Visando criar um produto com maior extensibilidade, reusabilidade e flexibilidade, deve ser adotar como linguagem principal de desenvolvimento Java seguindo cuidadosamente as técnicas de orientação a objetos. Entretanto, outras linguagens também poderão ser usadas quando indicações técnicas recomendem.

Prioridade: Essencial


**[NF004]** Portablilidade

O sistema deverá rodas em todos dispositivos que tenha o JAVA instalado


Prioridade: Importante