# Jogo de luta usando Web Services.


## Manual de Instalação

* `git clone <repository-url>` este repositório
* Entre dentro do diretório
* Entrar na pasta do projeto e abrir um prompt na pasta e executar `java -jar server.jar `
* Abrir outros dois prompts e executar `java -jar client.jar` em cada terminal aberto

### Comando para criar classes para ser usada no cliente do webservice:

* `wsimport -keep -p client_ws-s src/ localhost:8085/JogoLuta?wsdl`

## Manual do usuário (para jogar)

1. Quando o usuário entrar ele deve colocar o nome.
2. depois disso o sistema vai mostrar uma lista de comandos para ele com os comandos do jogo, após cada comando digitado é necessário dar `ENTER` para enviar o comando:

3. Depois que o segundo usuário entrar, o sistema irá falar quem é o primeiro a jogar.
4. Logo que o primeiro jogar, a mensagem com a vez do oponente será gerada.
5. E isso vai repetir até que haja algum ganhador e mostre quem ganhou.
6. Reseta os valores da pontuação, e reinicia com os mesmos jogadores e volta ao passo `2`.

`comandos:`

  ```
  C -> Chute
  S -> Soco
  D -> Defesa
  P -> Pontuação

  ```
<h4 align="center">Interface Do Jogo Em Execução</h4>
<div align="center"><img src="documentacao/jogo-em-execucao.jpg" height="800px" ></div>

## [Documento de Requisitos](documentacao/documentoRequisitos.md)

## [Diagrama de Sequência](documentacao/diagrama-de-sequencia.png)